(function ($, Drupal, window, document, undefined) {

        Drupal.behaviors.emailFieldText = {
                attach: function (context) {
                        if (context != document) return;

                        var emailField = "#edit-mailchimp-lists-mailchimp-cfpa-main-mergevars-email";

                        $(emailField).val('Email Address');
                }
        }

        Drupal.behaviors.emailFieldclear = {
                attach: function () {
                        var emailField = "#edit-mailchimp-lists-mailchimp-cfpa-main-mergevars-email";

                        $(emailField).one("focus", function() {
                                $(emailField).val("");
                        });
                }
        }

		Drupal.behaviors.sizeCalendar = {
			attach: function (context) {
				if (context != document) return;

				var cal = $(".calendar-calendar table.mini");
				var calHeight;
					
				calHeight = cal.width() * 0.7;
				cal.css('height', calHeight);

				$(window).resize(function() {
					calHeight = cal.width() * 0.7;
					cal.css('height', calHeight);
				});
			}
		}

		Drupal.behaviors.easydropdown = {
			attach: function () {
				var $selects = $('.events_calendar_filters select');

				$selects.easyDropDown({
					wrapperClass: 'easydropdown',
				});
			}
		}
}) (jQuery, Drupal, this, this.document);
